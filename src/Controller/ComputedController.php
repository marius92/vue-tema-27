<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ComputedController extends AbstractController
{
    /**
     * @Route("/computed", name="computed")
     */
    public function index()
    {
        return $this->render('computed/index.html.twig', [
            'controller_name' => 'ComputedController',
        ]);
    }

    /**
     * @Route("/namelist", name="namelist")
     */
     public function nameList(){
         $response = new Response();
         $response -> setContent(json_encode(
            ['names' => 
             [
            "Marius",
            "Saron",
            "Denisa",
            "Alin"
            ]]));
         $response->headers->set('Content-Type', 'application/json');
         return $response;
     }
}
